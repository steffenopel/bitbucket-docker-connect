(defproject docker-connect "0.1.0-SNAPSHOT"
  :description "Bitbucket Connect plugin for Docker Hub"
  :url "https://bitbucket.org/ssmith/bitbucket-docker-connect"
  :license {:name "Apache License 2.0"
            :url "http://www.apache.org/licenses/LICENSE-2.0"}

  :main docker-connect.core
  
  :source-paths ["src" "src/clojure"]
  :test-paths ["test" "test/clojure"]

  :plugins [[lein-environ "1.0.0"]]

  :exclusions [slf4j-log4j12
               org.slf4j/slf4j-log4j12
               log4j]
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/data.json "0.2.6"]
                 [environ "1.0.0"]

                 [ch.qos.logback/logback-core "1.1.3"]
                 [ch.qos.logback/logback-classic "1.1.3"]
                 [ch.qos.logback.contrib/logback-jackson "0.1.2"]
                 [com.fasterxml.jackson.core/jackson-databind "2.6.1"]
                 [ch.qos.logback.contrib/logback-json-classic "0.1.2"]

                 [ring "1.4.0"]
                 [ring/ring-defaults "0.1.5"]
                 [ring/ring-json "0.4.0"]
                 [com.duelinmarkers/ring-request-logging "0.2.0"]
                 [compojure "1.4.0"]
                 [org.immutant/web "2.1.0"]

                 [selmer "0.9.1"]

                 [circleci/clj-yaml "0.5.4"]
                 [clj-http "2.0.0"]
                 [clj-connect "0.1.4"]

                 [com.taoensso/faraday "1.7.1"]]

  :profiles {:dev
             {:jvm-opts ["-Dlog.appender=simple"]

              ;; Copy these into profiles.clj and update
              :env {:base-url "OVERRIDE"
                    :oauth-key "OVERRIDE"
                    :project-key "docker-connect"
                    :project-name "Bitbucket Connect Docker Hub plugin"
                    :db-endpoint "http://localhost:8000"}}

             :uberjar
             {:aot :all}})
