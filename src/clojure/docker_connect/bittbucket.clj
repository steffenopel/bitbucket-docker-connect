(ns docker-connect.bittbucket
  (:require [clojure.tools.logging :as log]
            [clj-time.format :as tf]
            [clj-http.client :as http]
            [clj-yaml.core :as yaml]
            [clj-connect.jwt :as jwt]
            [docker-connect.util :refer :all]))


(defn get-user-display-name [oauth]

  (let [data (http/get "https://bitbucket.org/api/1.0/user/" {:oauth-token oauth
                                                                :as :json})]
    (log/info "Received user data:" (pps data))
    (-> data
        :body
        :user
        :display_name)))


(defn- parse-file [text]
  (some->> text
           yaml/parse-string
           (into {})))

;; Find the corresponding docker repository for bitbucket one.
;; Honours "repository:" setting in ".docker-repository.yml",
;; otherwise defaults to the Bitbucket repo path.
(defn get-docker-repository [bb-repo]
  ;; FIXME: This is currently unauthenticated.
  ;; FIXME: This needs caching.
  (let [resp (http/get (str "https://bitbucket.org/api/1.0/repositories/" bb-repo "/raw/HEAD/.docker-repository.yml")
                       {:throw-exceptions false})]

    ;; Only enable the plugin if the file is present. Return nil otherwise.
    ;; If file doesn't contain a Docker repo name then use the BB one.
    (cond
      (not (ok-or-nil resp))
      nil

      :else
      (let [file (parse-file (:body resp))]
        (:repository file bb-repo)))))


;; Example:
;;
;; {
;;     "author": {
;;         ...
;;     },
;;     "date": "2015-09-01T12:22:52+00:00",
;;     "hash": "0c40e803008b0261f0d7afa22f108f0221724174",
;;     "links": {
;;         ...
;;     },
;;     "message": "Add docker-repository YAML file\n",
;;     "parents": [
;;         {
;;             "hash": "39faa0d72aeeebacd224fcf720fdb319651ad4cb",
;;             "links": {
;;                 ...
;;             },
;;             "type": "commit"
;;         }
;;     ],
;;     "repository": {
;;         "full_name": "ssmith/docker-multipy",
;;         "links": {
;;             ...
;;         },
;;         "name": "docker-multipy",
;;         "type": "repository",
;;         "uuid": "{8ab76ea4-b5f5-4763-bd53-eea98a6be54b}"
;;     },
;;     "type": "commit"
;; }
(defn last-commit [repo branch]
  (let [resp (http/get (str "https://bitbucket.org/api/2.0/repositories/" repo "/commits/" branch)
                       {:as :json
                        :throw-exceptions false})]

    (some-> (ok-or-nil resp)
            :values
            first
            (update :date tf/parse))))


(defn fetch-oauth-token [addon-ctx]
  (let [token (jwt/gen-jwt-token
               "POST" "/site/oauth2/access_token" {}
               (addon-ctx "key") (addon-ctx "clientKey") (addon-ctx "sharedSecret"))
        resp (http/post "https://bitbucket.org/site/oauth2/access_token"
               {:headers {"Authorization" (str "JWT " token)}
                :form-params {:grant_type "urn:bitbucket:oauth2:jwt"}
                :as :json})]
    (-> resp
        :body
        :access_token)))

