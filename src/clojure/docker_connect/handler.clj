(ns docker-connect.handler
  (:require [clojure.string :as string]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.tools.logging :as log]

            [clj-time.core :as tm]

            [clj-connect.jwt :as jwt]

            [docker-connect.util :refer :all]
            [docker-connect.docker :as docker]
            [docker-connect.bittbucket :as bb]
            [docker-connect.storage :as store]

            [ring.util.response :as response]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.middleware.json :refer [wrap-json-body wrap-json-response]]
            [com.duelinmarkers.ring-request-logging :refer (wrap-request-logging)]
            [compojure.core :refer :all]
            [compojure.route :as route]

            [clj-http.client :as http]

            [environ.core :refer [env]]
            [selmer.parser :refer [render-file]]))


;; FIXME: Should be a Ring middleware? Or Friend-JWT?
(defn wrap-jwt-auth [request handler]
  (let [token (jwt/extract-token request)
        secret (store/shared-secret (-> token :claims :iss))]
    (if (not (jwt/verify-token token secret))
      {:status 401}
      (handler (request :query-params) (request :body)))))


(defn log-installs []
  (log/info "Current installed total is" (store/installed-count)))


(defn gen-descriptor [params body]
  (log/info "Received descriptor request")

  ;; Fetch configuration from the environment (see `environ` docs)
  (let [ctx {:base-url (env :base-url)
             :oauth-key (env :oauth-key)
             :project-key (env :project-key)
             :project-name (env :project-name)}]

    {:status 200
     :headers {"Content-Type" "application/json; charset=utf-8"}
     :body (render-file "views/atlassian-connect.json.selmer" ctx)}))


(defonce ^:const status-default {:name "Unknown"    :color "red"})
(defonce ^:const status-map {-2 {:name "Exception"  :color "red"}
                             -1 {:name "Error"      :color "red"}
                              0 {:name "Pending"    :color "orange"}
                              1 {:name "Claimed"    :color "orange"}
                              2 {:name "Started"    :color "orange"}
                              3 {:name "Cloned"     :color "orange"}
                              4 {:name "Readme"     :color "orange"}
                              5 {:name "Dockerfile" :color "orange"}
                              6 {:name "Built"      :color "orange"}
                              7 {:name "Bundled"    :color "orange"}
                              8 {:name "Uploaded"   :color "orange"}
                              9 {:name "Pushed"     :color "orange"}
                             10 {:name "Complete"   :color "green"}})

(defn- get-build-meta [docker-repo]
  (some-> (docker/latest-build docker-repo)
          (update :status #(assoc (status-map % status-default) :id %))))

(defn- is-fresh? [build docker-repo bb-repo]
  (let [branch (some-> (docker/autobuild-meta docker-repo)
                   (get-in [:build_tags :source_name]))
        commit (bb/last-commit bb-repo branch)]

    (tm/after? (:last_updated build)
               (:date commit))))

(defn get-repo-page [params body]
  (let [repo-path (params "repoPath")
        docker-repo (bb/get-docker-repository repo-path)
        meta (when docker-repo (docker/fetch-metadata docker-repo))]

    (cond
      ;; Couldn't fetch repository metadata; possibly due to permissions?
      (nil? docker-repo)
      (do
        (log/info "BB Repo" repo-path "is not enabled.")
        {:status 200
         :headers {"Content-Type" "text/html; charset=utf-8"}
         :body (render-file "views/no-bb-meta.html.selmer" {})})

      ;; No metadata from Docker, return error page
      (nil? meta)
      (do
        (log/info "Docker repo" docker-repo "metadata lookup failed.")
        {:status 200
         :headers {"Content-Type" "text/html; charset=utf-8"}
         :body (render-file "views/no-repository.html.selmer" {})})

      ;; Got all the info, let's go
      :else
      (let [build (get-build-meta docker-repo)
            utd (when build (is-fresh? build docker-repo repo-path))]
        (log/info "Displaying for" repo-path "/" docker-repo)
        {:status 200
         :headers {"Content-Type" "text/html; charset=utf-8"}
         :body (render-file "views/docker-repository.html.selmer"
                            (merge {:docker-user (meta :user)
                                    :docker-repo docker-repo
                                    :docker-public (if (meta :is_private) "Private" "Public")
                                    :docker-stars (meta :star_count)
                                    :docker-pulls (meta :pull_count)
                                    :docker-build build
                                    :build-utd (if utd
                                                 {:color "green" :name "Up to date"}
                                                 {:color "orange" :name "Out of date"})}))}))))


(defn process-installed [params body]
  (log/info "Received /installed of:\n" (pps body))
  (store/save-install-context body)
  (log-installs)

  {:status 204})

(defn process-uninstalled [params body]
  (log/info "Received /uninstalled of:\n" (pps body))

  (store/delete-install-context body)
  (log-installs)

  {:status 204})

(defn process-webhook [params body]
  (log/info "Received /webhook of:\n" (pps body))  
  {:status 204})


(defroutes app-routes
  ;; By default we serve the Connect descriptor
  (GET  "/" [] (response/redirect "/atlassian-connect.json"))

  ;; Bitbucket connect operations
  (GET  "/atlassian-connect.json" {params :query-params body :body}
        (gen-descriptor params body))

  (POST "/installed" {params :query-params body :body}
        (process-installed params body))

  (POST "/uninstalled" request
        (wrap-jwt-auth request process-uninstalled))
  
  (POST "/webhook" request
        (wrap-jwt-auth request process-webhook))
  
  (GET  "/docker-repository" request
        (wrap-jwt-auth request get-repo-page))


  ;; Micros operations
  (GET  "/healthcheck" request
        {:status 200 :body {:status "OK"}})


  ;; Static file support. Will serve anything under :resources
  (route/resources "/")
  (route/not-found {:status 404 :body "Not Found"}))


(def app
  ;; Disable anti-forgery as it interferes with Connect POSTs
  ;; FIXME: Should be api-defaults? We're mixing HTML and REST here.
  (let [connect-defaults (-> site-defaults
                             (assoc-in [:security :anti-forgery] false)
                             (assoc-in [:security :frame-options] false)
                             (assoc-in [:proxy] true)) ]

    (-> app-routes
        (wrap-defaults connect-defaults)
        (wrap-json-body)
        (wrap-json-response)
        (wrap-request-logging))))


(defn init-environment []
  (log/debug "Got environment of " (pps env))

  ;; Set proxy hosts; this is primarily for Micros.
  (when (env :http-proxy-host)
    (log/info "Setting proxies of "
              (env :http-proxy-host)
              (env :http-proxy-port)
              (env :https-proxy-host)
              (env :https-proxy-port)
              (env :no-proxy)
              (env :no-proxy-jvm))
    
    (System/setProperty "http.proxyHost" (env :http-proxy-host))
    (System/setProperty "http.proxyPort" (env :http-proxy-port))
    (System/setProperty "http.nonProxyHosts" (env :no-proxy-jvm))
    (System/setProperty "https.proxyHost" (env :https-proxy-host))
    (System/setProperty "https.proxyPort" (env :https-proxy-port))
    (System/setProperty "https.nonProxyHosts" (env :no-proxy-jvm))))


(defn init []
  (log/info "Initialising application")
  (init-environment)
  (store/init-storage)

  (log-installs))


(defn shutdown []
  (log/info "Shutting down application"))
