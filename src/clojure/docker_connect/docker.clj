(ns docker-connect.docker
  (:require [clj-http.client :as http]
            [clojure.tools.logging :as log]
            [clj-time.format :as tf]
            [docker-connect.util :refer :all]))


;; Example:
;;
;;{:description "A Docker image containing multiple Python versions for testing",
;;  :star_count 0,
;;  :is_private false,
;;  :can_edit false,
;;  :name "multipy",
;;  :full_description "The source Dockerfile is available ....",
;;  :last_updated "2015-08-07T16:49:06.471084Z",
;;  :is_automated false,
;;  :status 1,
;;  :has_starred false,
;;  :pull_count 25,
;;  :namespace "tarkasteve",
;;  :user "tarkasteve"}
;;
(defn fetch-metadata [repo]
  ;; FIXME: Assumes public repository for now
  (log/info "Fetching Docker Hub data for" repo)

  (let [resp (http/get (str "https://hub.docker.com/v2/repositories/" repo)
                       {:as :json
                        :throw-exceptions false})]
    (ok-or-nil resp)))


;; Example
;;
;; {:id 1860298,
;;  :status 10,
;;  :tag 129159,
;;  :created_date "2015-09-06T11:06:16.445729Z",
;;  :last_updated "2015-09-06T11:21:45.404533Z",
;;  :build_code "bgy6sccqjmtwzrhlg5fkkhf"}
;;
(defn latest-build [repo]
  ;; FIXME: Assumes public repository for now
  (log/info "Fetching Docker Hub builds for" repo)

  (let [resp (http/get (str "https://hub.docker.com/v2/repositories/" repo "/buildhistory/")
                       {:as :json
                        :throw-exceptions false})]

    (some-> (ok-or-nil resp)
            :results
            first
            (update :created_date tf/parse)
            (update :last_updated tf/parse))))

;; Example:
;;
;; {
;;     "active": true,
;;     "build_name": "ssmith/docker-multipy",
;;     "build_tags": [
;;         {
;;             "dockerfile_location": "/",
;;             "id": 129159,
;;             "name": "latest",
;;             "source_name": "master",
;;             "source_type": "Branch"
;;         }
;;     ],
;;     "deleted": false,
;;     "deploykey": null,
;;     "docker_url": "tarkasteve/multipy",
;;     "hook_id": null,
;;     "provider": "bitbucket",
;;     "repo_id": "docker-multipy",
;;     "repo_type": "git",
;;     "repo_web_url": "https://bitbucket.org/ssmith/docker-multipy",
;;     "repository": 312467,
;;     "source_url": "https://bitbucket.org/ssmith/docker-multipy.git",
;;     "user": "tarkasteve"
;; }
(defn autobuild-meta [repo]
  ;; FIXME: Assumes public repository for now
  (log/info "Fetching autobuild data for" repo)

  (let [resp (http/get (str "https://hub.docker.com/v2/repositories/" repo "/autobuild/")
                       {:as :json
                        :throw-exceptions false})]

    (ok-or-nil resp)))
