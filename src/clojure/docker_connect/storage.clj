(ns docker-connect.storage
  (:require [clojure.tools.logging :as log]
            [taoensso.faraday :as dyn]
            [environ.core :refer [env]]
            [docker-connect.util :refer :all])
  (:import com.amazonaws.auth.InstanceProfileCredentialsProvider))

(defonce storage-ctx (merge {}
                            (when (env :http-proxy-host)
                              {:proxy-host (env :http-proxy-host)
                               :proxy-port (read-string (env :http-proxy-port))})

                            (when (in-micros?)
                              {:provider (InstanceProfileCredentialsProvider.)
                               :endpoint (str "http://dynamodb." (env :dynamo-installed-table-region) ".amazonaws.com")})

                            (when (env :db-endpoint)
                              {:access-key ""
                               :secret-key ""
                               :endpoint (env :db-endpoint)})))

(defonce installed-table (or (env :dynamo-installed-table-name)
                             :installed))

(defonce tracking-id "meta:tracking")

(defn create-table []
  (dyn/create-table storage-ctx installed-table
                    [:id :s]
                    {:throughput {:read 10 :write 10}
                     :block? true}))

(defn check-metadata []
  (try
    (dyn/put-item storage-ctx installed-table
                  {:id tracking-id
                   :install-count 0
                   :uninstall-count 0}
                  {:expected {:id :not-exists}})
    (catch #=(dyn/ex :conditional-check-failed) e
           (log/info "Metadata already exists, skipping create"))))

(defn installed-count []
  (let [tracking (dyn/get-item storage-ctx installed-table {:id tracking-id})]
    (int (reduce - (keep tracking [:install-count :uninstall-count])))))

(defn inc-installed []
  (dyn/update-item storage-ctx installed-table {:id tracking-id}
                   {:install-count [:add 1]}))

(defn inc-uninstalled []
  (dyn/update-item storage-ctx installed-table {:id tracking-id}
                   {:uninstall-count [:add 1]}))

(defn init-storage []
  (log/info "Intitialising storage connection" (pps storage-ctx))
  (log/info "Installed table is" installed-table)

  (when (in-dev?)
    ;; Micros doesn't allow table-level operations
    (log/info "DEV: Checking for table...")
    (let [tables (dyn/list-tables storage-ctx)]
      (log/info "Got storage tables of" tables)

      ;; The following should only be true in dev
      (when (not-any? #(= installed-table %) tables)
        (log/info "No table available, creating")
        (create-table))))

  (check-metadata))


(defn shared-secret [client-key]
  ;; FIXME: Cache?
  (:shared-secret (dyn/get-item storage-ctx installed-table {:id client-key}
                                {:attrs [:shared-secret]})))

(defn save-install-context [context]
  (dyn/put-item storage-ctx installed-table
                {:id (context "clientKey")
                 :shared-secret (context "sharedSecret")
                 :install-username ((context "principal") "username")
                 ;; Dump of the context in-case we need it
                 :context (dyn/freeze context)})

  (inc-installed))

(defn delete-install-context [context]
  (dyn/delete-item storage-ctx installed-table
                   {:id (context "clientKey")})
  (inc-uninstalled))
