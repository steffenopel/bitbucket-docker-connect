
(ns docker-connect.util
  (:require [clojure.pprint :refer [pprint]]
            [environ.core :refer [env]]))


;; Pretty-print to a string for logging
(defn pps [form]
  (with-out-str (pprint form)))

(defn in-micros? []
  (contains? env :micros-service))

(defn in-dev? []
  (not (in-micros?)))


(defn ok-or-nil [response]
  (if (= 200 (response :status))
    (response :body)
    nil))
