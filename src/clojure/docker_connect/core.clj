(ns docker-connect.core
  (:require [clojure.tools.logging :as log]
            [docker-connect.handler :refer [app init shutdown]]
            [immutant.web :as web]
            [environ.core :refer [env]])
  (:gen-class))


(defonce server (atom nil))
(defonce host (env :host "0.0.0.0"))
(defonce port (env :port 8080))

(defn start-server [port]
  (init)
  (reset! server (web/run app :host host :port port)))

(defn stop-server []
  (when @server
    (shutdown)
    (web/stop @server)
    (reset! server nil)))

(defn start-app []
  (.addShutdownHook (Runtime/getRuntime) (Thread. stop-server))
  (start-server port)
  (log/info "server started on:" (:host @server) (:port @server)))

(defn -main [& args]
  (start-app))
