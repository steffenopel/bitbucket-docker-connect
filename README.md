# Bitbucket Docker Connect

A Bitbucket Connect plugin for Docker Hub.

![Screenshot](doc/screenshot.png)

## Usage

The Docker Hub Connect plugin links-to and displays the state of a corresponding
image repository on Docker hub. If the image has autobuild enabled the
information about the latest build will also be displayed.

### Configuring the Hub repository

The plugin is enabled by placing the file `.docker-repository.yml` in root of
your repository.

By default the plugin will assume that your Bitbucket and Docker Hub
accounts/repositories have the same names; e.g. if your Bitbucket repository
is:

    https://bitbucket.org/ssmith/mydocker

the corresponding Hub repository is:

    https://hub.docker.com/ssmith/mydocker

You can override this default by placing a file called
`.docker-repository.yml` in root of the Bitbucket repository. This is
[YAML](https://en.wikipedia.org/wiki/YAML) file that contains a single
entry `repository` which should contain the account/repository
override (e.g. `myhubaccount/docker-repo`. For example:

    ---
    repository: tarkasteve/multipy

### Currently limitations/future work

* Both the Bitbucket and Hub repositories must be public. Private
  repositories may be supported in the future.
* Private registries are not supported, this may change in a future
  version.

## License

Copyright © 2015 Atlassian

Distributed under the Apache License 2.0.
