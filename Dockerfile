FROM java:8

COPY target/docker-connect-*-standalone.jar /opt/docker-connect.jar

EXPOSE 8080

# Never run as root!
RUN adduser --disabled-password --gecos '' connect

USER connect
WORKDIR /opt

CMD java -Xmx2048m -Xms512m -jar docker-connect.jar
